## :rocket: Sobre o desafio

Desenvolvimento de um app para criação de todo list.

## **Um pouco sobre as ferramentas**

Durante a resolução do desafio, utilizei as ferramentas :

- ReactJS
- Yarn
- ESLint
- Prettier
- EditorConfig
- Yup
- Styled-components
- Unform

## **Como instalar o projeto na sua máquina**

1. Clone o repositório em sua máquina.
2. Instale as dependecias do projeto :&nbsp;&nbsp;&nbsp; `yarn add`&nbsp; ou &nbsp; `npm install`
3. Após finalizar as configurações, execute no seu terminal `yarn dev`
