import React, { useEffect, useState } from 'react';
import { Form, Input } from '@rocketseat/unform';
import * as Yup from 'yup';

import image from '../../assets/todo-image.svg';
import { Container, Content } from './styles';

const schema = Yup.object().shape({
  todo: Yup.string()
    .required('O campo to-do é obrigatório')
    .min(5, 'O to-do precisa de no mínimo 5 caracteres'),
});

export default function SignIn() {
  const [value, setValue] = useState('');
  const [todos, setTodos] = useState([]);

  function handleSubmit() {
    setTodos([...todos, value]);
    setValue('');
    localStorage.setItem('list_todos', JSON.stringify([...todos, value]));
  }

  useEffect(() => {
    setTodos(JSON.parse(localStorage.getItem('list_todos')) || []);
  }, []);

  return (
    <>
      <Container>
        <h1>To-do App</h1>
        <Form schema={schema} onSubmit={handleSubmit}>
          <Input
            name="todo"
            type="text"
            value={value}
            onChange={e => setValue(e.target.value)}
            placeholder="Digite um To-do"
          />
          <button type="submit">Adicionar</button>
        </Form>
        <Content>
          <ul>
            {todos.map((todo, index) => (
              <li key={index}>{todo}</li>
            ))}
          </ul>
          <div>
            <img src={image} alt="todo" />
          </div>
        </Content>
      </Container>
    </>
  );
}
