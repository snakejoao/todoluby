import styled from 'styled-components';

export const Container = styled.div`
  height: 60%;
  justify-content: center;
  align-items: center;
  @media only screen and (max-height: 736px) {
    position: relative;
    top: 16%;
  }
  h1 {
    font-size: 54px;
    color: #6d44b8;
    border-bottom: 3px solid;
    text-align: center;
    margin: 10px;
  }

  form {
    border-radius: 3px;
    padding-top: 25px;
    padding: 0 25px;
    padding-bottom: 30px;
    display: flex;
    flex-direction: column;
    margin-top: 30px;
    flex-wrap: wrap;
    max-width: 400px;
    margin: 0 auto;
    justify-content: center;
    margin-top: 40px;
  }

  span {
    color: red;
    align-self: flex-start;
    margin: 5px;
    font-weight: bold;
    font-size: 15px;
  }

  input {
    text-align: center;
    border-radius: 4px;
    border: 2px solid #a580eb;
    padding: 5px;
    height: 40px;
  }

  button {
    margin: 5px 0 0;
    font-weight: bold;
    border-radius: 4px;
    font-size: 16px;
    background-color: #6d44b8;
    max-width: 400px;
    height: 40px;
    transition: 400ms;
    color: #f0f0f5;
    border: 2px solid #9774d8;
    cursor: pointer;

    &:hover {
      background-color: #9165e2;
    }

    @media only screen and (max-width: 736px) {
      padding: 10px;
    }
  }
`;

export const Content = styled.div`
  display: flex;

  ul {
    background-color: #6d44b8;
    border-radius: 10px;
    border: 4px solid #9165e2;
    margin-top: 40px;
    text-align: center;
    width: 500px;
    height: 385px;
    overflow: auto;
  }

  li {
    margin: 25px;
    font-size: 15px;
    color: #f0f0f5;
  }

  a {
    padding: 5px;
    margin-left: 10px;
    border-radius: 10px;
    transition: 400ms;
    color: #f0f0f5;
    border: 2px solid #9774d8;
    cursor: pointer;

    &:hover {
      background-color: #9165e2;
    }
  }

  div {
    width: 100%;
    position: fixed;
    right: 50%;
    left: 48%;

    img {
      width: 48%;
    }
  }
`;
