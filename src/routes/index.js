import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Todo from '../pages/Todo/index';

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Todo} />
    </Switch>
  );
}
