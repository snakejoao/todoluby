import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@700&family=Ubuntu:wght@500&display=swap');
* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

html {
  font-family: Roboto, sans-serif;
  height: 100%;
  width: 90%;
  margin: 0 auto;
}

body {
  background: #f0f0f5;
  -webkit-font-smoothing: antialiased;
}

a, ul, button, input {
  text-decoration: none;
  list-style: none;
  outline: 0;
  font-family: roboto, sans-serif;
  color: #5f5e5e;
}

h1, h2, h3, h4, h5, h6 {
  font-family: Ubuntu, sans-serif;
}
`;
